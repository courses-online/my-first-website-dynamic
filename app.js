var express=require('express');
var mongoose= require('mongoose');
var bodyParser= require('body-parser');
var multer = require('multer');
var cloudinary = require('cloudinary');
var app_password="1234";
var method_override = require('method-override');
var Schema=mongoose.Schema;

cloudinary.config({ 
  cloud_name: 'trossky', 
  api_key: '436331721733974', 
  api_secret: 'Xe7b8oOjUZLAnc2rhcNrNGSiyu4' 
});


var app= express();

mongoose.connect("mongodb://localhost/primera_pagina");

//Aplicamos pase al cuerpo de la solicitud Post
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(method_override("_method"));

var uploader = multer({dest: "./uploads"});
var middleware_upload = uploader.single('image_avatar');
//Objeto JSON => javaScript Object Notation
//Definimos el esquema de Courseos
var courseSchemaJSON={
	title:String,
	description:String,
	imageUrl: String,
	pricing: Number
};
var courseSchema = new Schema(courseSchemaJSON);
//Agrego propiedades virtuales a los datos de la base de datos.
//Creacion de Attributo Virtual (Getter->Obtener atributos)
courseSchema.virtual("image.url").get(function(){
	if(this.imageUrl == "" || this.imageUrl == "data.png" ){
		return "default.jpg";
	}
	return this.imageUrl;
});

var Course= mongoose.model("Course",courseSchema);
app.set("view engine","jade");

//Creamos los asset
app.use(express.static("public"));

app.get("/",function(solicitud,respuesta){
	respuesta.render("index");
});
app.get("/courses",function(solicitud,respuesta){
	Course.find(function(error,documento){
		if(error){ console.log(error);}
		respuesta.render("courses/index",{courses:documento})
	});

});
//Seleccion de Curso a Editar
app.get("/courses/edit/:id",function(solicitud,respuesta){
	var id_course=solicitud.params.id;
	Course.findOne({"_id": id_course},function(error,curso){
		console.log(curso);
		respuesta.render("courses/edit",{course: curso});
	});

});
//Actualizar Curso
app.put("/courses/:id",uploader.single('image_avatar'),function(solicitud,respuesta){
	
	if(solicitud.body.password==app_password){	
	
		var data={
			title:solicitud.body.title,
			description:solicitud.body.description,
			pricing: solicitud.body.pricing
		};
		if(solicitud.file){
	    	cloudinary.uploader.upload(solicitud.file.path,function(result) {
	            data.imageUrl = result.url;

	            Course.update({"_id": solicitud.params.id},data,function(course){
					respuesta.redirect("/courses");
				});

	        });

	        

		}else{
			Course.update({"_id": solicitud.params.id},data,function(course){
				respuesta.redirect("/courses");
			});

		}

	}else{
		respuesta.redirect("/");
	}

});
//Direccion de Administrador
app.get("/admin",function(solicitud,respuesta){
	respuesta.render("admin/form");
});
//respuesta al formulario adminsitrador para ver los cursos
app.post("/admin",function(solicitud,respuesta){
	if(solicitud.body.password==app_password){
		Course.find(function(error,documento){
		if(error){ console.log(error);}
		respuesta.render("admin/index",{courses:documento})
		});

	}else{
		respuesta.redirect("/");
	}
	
});
//Creacion de Cursos Nuevos, con sus imagenes


app.post("/courses",middleware_upload,function(solicitud,respuesta){

	if(solicitud.body.password==app_password){	
		var data={
			title:solicitud.body.title,
			description:solicitud.body.description,
			pricing: solicitud.body.pricing
		}
		var course= new Course(data);

		if(solicitud.file){
	    	cloudinary.uploader.upload(solicitud.file.path,function(result) {
	            course.imageUrl = result.url;
		            course.save(function(err){
		                respuesta.redirect("/courses");
		                
		        });
	        });
		}else{
			course.save(function(err){
				respuesta.redirect("/courses");
			});
		}
	}else{
			respuesta.render("courses/new");

	}	
});
//Acedemos a la vista 
app.get("/courses/new",function(solicitud,respuesta){
	respuesta.render("courses/new");

});
app.get("/courses/:id/delete",function(solicitud,respuesta){
	var id=solicitud.params.id;
	Course.findOne({"_id": id},function(error,curso){
		respuesta.render("courses/delete",{course: curso});
	});

});

app.delete("/courses/:id",uploader.single('image_avatar'),function(solicitud,respuesta){
	var id=solicitud.params.id;
	if(solicitud.body.password==app_password){
		Course.remove({"_id": id},function(err){
			if(err){console.log(err);}
			respuesta.redirect("/admin");
		});
	}else{
		respuesta.redirect("/courses");
	}

});

app.listen(9090);
